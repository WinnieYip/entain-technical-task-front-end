import moment from "moment";

// Neds endpoint
export const NEXT_RACES_GET_ENDPOINT =
  "https://api.neds.com.au/rest/v1/racing/?method=nextraces&count=10";

// Category IDs for various types of racing
export const CATEGORIES = {
  "9daef0d7-bf3c-4f50-921d-8e818c60fe61": "Greyhound Racing",
  "161d9be2-e909-4326-8c2c-35ed71fb460b": "Harness Racing",
  "4a2788f8-e825-4d36-9894-efd4baf1cfae": "Horse Racing",
};

// Time elapsed
export const ONE_MINUTE = -60;

// Function to obtain the seconds remaining until a given Epoch Time
export const getSecondsRemainingUntilEpochTime = (givenEpochTime) => {
  return givenEpochTime - moment().unix();
};

// Races to display in the table
export const RACES_TO_DISPLAY = 5;
