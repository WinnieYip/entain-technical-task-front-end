# Entain Technical Task Front-End

## Acknowledgements

I decided whilst trying my hand out at Vue that it would be worthwhile testing out the Ant Design component library which I have never used before in any fornt-end framework. The two sources below were instrumental in helping achieve the goals of this task.

https://www.antdv.com/components/table/
https://codesandbox.io/s/9jeu2?file=/src/App.vue

## Additional Comments

To filter by category, there is a filter icon on the top right hand side of the category header which will allow you to toggle between Greyhound Racing, Horse Racing and Harness Racing.

The data has automatically been sorted by advertised start times by ascending order i.e. the oldest dates first and the future dates further down. This can be toggled in the Advertised Start header.

This application is best run in Chrome.

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Lints and fixes files

```
npm run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
